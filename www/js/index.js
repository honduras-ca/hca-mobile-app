/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var controller;
var app = {
    // Application Constructor
    self: null,
    databaseInteractor: null,

    initialize: function() {  
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        self = this;
    },

    // deviceready Event Handler
    onDeviceReady: function deviceReady() {
        //preliminary initialize the database
        console.log("The device is ready!");
        DatabaseInteractor(function(err, databaseInteractorReceived){
            if(!err){
                console.log("Trying to onDbInitialize!");
                self.databaseInteractor = databaseInteractorReceived;
                self.onDbInitialize();
            }
            else{
                deviceready();
            }
        });
        
    },

    onDbInitialize: function readyLogin() {
        //try to login
        login(self.databaseInteractor, function(err){
            if(!err){
                //use the app!
                controller = new Controller(self.databaseInteractor);
             }
             else{
                readyLogin();
            }
        });
    }

};

app.initialize();
