var Controller = function(databaseInteractorInput) {
	var controller = {
		self: null,
		databaseInteractor: databaseInteractorInput,

		initialize: function(){
			console.log("Controller created!");
			self = this;
			self.registerClicks();
			self.registerEvents();
			$.mobile.pageContainer.pagecontainer("change", "#home");
		},

		registerClicks: function(){
			//handles logout button click
			$('#homeHeader').on("click", "#logout", function(){
				logout(false, self.databaseInteractor, function(err){
					if(!err){
   						self.buttonLogin();
					}
					else{
    					console.log("Logout button error");
					}
				});
			});

			//handles sync button click
			$('#homeHeader').on("click", "#sync", function(){
				console.log("You are telling me to sync");
				navigator.notification.confirm("Are you sure you want sync.", function(res){
					if(res == 1){
						$.mobile.loading("show", {text: "Sync Loading...", textVisible: "true", theme: "a", html:""});
						self.databaseInteractor.sync(function(err){
							if(!err){
								self.renderHomepage();
								$.mobile.loading("hide");
								navigator.notification.alert("Sync succeeded!", function(){}, 'Sync');
							}
							else{
		    					$.mobile.loading("hide");
		    					navigator.notification.alert("There was an error. Please restart the app and try again!", function(){}, 'Sync');				
		    				}
						});
					}
					else{
						console.log("They did not logout!");
					}
				}, "Sync", ["Yes", "No"]);
			});

			//when the home button is clicked in the navbar
			$('.navbar').on("click", "#homeNavbar", function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer("change", "#home");
			});

			//when the student button is clicked in the navbar
			$('.navbar').on("click", "#studentsNavbar", function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer("change", "#students");
			});

			//when one of the dynamically loaded attendance records is clicked on the home page
			$('#homeBody').on('click', '.edit-attendance-button', function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer("change", "#editAttendance", {reportUID: parseInt($(this).attr("href"))});
			});

			$('#homeBody').on('click', '#createNewReport', function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer("change", "#editAttendance");
			});

			$('#studentsBody').on('click', '.editStudentButton', function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer('change', "#editStudent", {studentUID: parseInt($(this).attr("href"))});
			});

			//when the add report button is clicked on the editAttendance page
			$('#attendanceBody').on('click', '#addStudentToReport', function(event){
				event.preventDefault();
				var uid = $('#reportUID').html();
				$.mobile.pageContainer.pagecontainer("change", "#students", {reportUID: parseInt(uid)});
			});

			//adds a student to the report
			$('#studentsList').on('click', '.addThisStudentToReport', function(event){
				event.preventDefault();
				var uid = parseInt($('#studentReportUID').html());
				var studentUID = parseInt($(this).attr("href"));
				self.databaseInteractor.addStudentToReport(uid, studentUID, function(err){
					if(!err){
						$.mobile.pageContainer.pagecontainer("change", "#editAttendance", {reportUID: uid});
					}
				});
			});

			//adds a discipline point to the individual reocrd of the day
			$('#editAttendanceTable').on('click', '.addDisciplinePoint', function(event){
				event.preventDefault();
				var href = parseInt($(this).attr("href").slice(19));
				self.databaseInteractor.addDisciplinePoint(href, function(err){
					if(!err){
						self.databaseInteractor.getDisciplinePoints(href, function(err, disciplinePoints){
							if(!err){
								$('a[href="#addDisciplinePoint' + href + '"]').html(disciplinePoints);
							}
						});
					}
				});
			});

			//delete a student from the report
			$(document).on('click', '.deleteStudentFromReport', function(event){
				event.preventDefault();
				var href = $(this).attr("href");
				self.databaseInteractor.removeIndividualRecord(parseInt(href), function(err){
					if(!err){
						$('#recordUID' + href).remove();
						$('#otherPopup' + href).popup('close');
					}
				});
			});

			//when the edit student from the edit report page is clicked
			$(document).on('click', '.editStudentFromReport', function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer('change', "#editStudent", {studentUID: parseInt($(this).attr("href"))});
			});

			//save a comment
			$(document).on('click', '.saveComment', function(event){
				event.preventDefault();
				var href = parseInt($(this).attr("href"));
				self.databaseInteractor.setComment(href, $('#comments' + href).val(), function(err){
					if(!err){
						$('#otherPopup' + href).popup('close');
						console.log("Comment saved!");
					}
				});
			});

			//save ate
			$('#editAttendanceTable').on('change', '.ate', function(event){
				console.log($(this).prop("checked") ? 1 : 0);
				self.databaseInteractor.didEat(parseInt($(this).attr("id")), $(this).prop("checked") ? 1 : 0, function(err){
					if(!err){
						console.log("Ate change saved");
					}
				});
			});

			//change the snacks field
			$('#snacks').on('change', function(event){
				self.databaseInteractor.setSnack(parseInt($('#reportUID').text()), parseInt($(this).val()), function(err){

				});
			});

			//change the classtypes field
			$('#classtypes').on('change', function(event){
				self.databaseInteractor.setClassType(parseInt($('#reportUID').text()), parseInt($(this).val()), function(err){

				});
			});

			//change the classrooms field
			$('#classrooms').on('change', function(event){
				self.databaseInteractor.setClassroom(parseInt($('#reportUID').text()), parseInt($(this).val()), function(err){

				});
			});

			//change the teachers field
			$('#teachers').on('change', function(event){
				self.databaseInteractor.setTeachers(parseInt($('#reportUID').text()), $(this).val(), function(err){

				});
			});

			$('#reportComments').on('change', function(event){
				console.log("Yo!");
				self.databaseInteractor.setReportComments(parseInt($('#reportUID').text()), $(this).val(), function(err){

				});
			});

			//the function that handles the saving of an edited student
			$('#editStudentBody').on("click", "#saveStudentObj", function(event){
				event.preventDefault();
				// pushes newStudentObj to editStudent() to save changes made by mobile user
				var newStudentObj = {};

				var formData = $("#editStudentForm").serializeArray();
				$.each(formData, function(i, obj){
					if(obj.value == ""){
						obj.value = null;
					}
					newStudentObj[obj.name] = obj.value;
				});

				self.databaseInteractor.getStudent(newStudentObj.uid, function(err, oldStudentObj){
					if(!err){
						var modObj = {}
						var newKeys = Object.keys(newStudentObj);

						for(var i = 0; i < newKeys.length; i++){
							modObj[newKeys[i]] = {};
							modObj[newKeys[i]]["value"] = newStudentObj[newKeys[i]];

							//if the student object has a null or "" value in it
							if(newStudentObj[newKeys[i]] == "" || newStudentObj[newKeys[i]] == null){
								//if the old student object is also null or ""
								if(oldStudentObj[newKeys[i]] == "" || newStudentObj[newKeys[i]] == null){
									//it has not change
									modObj[newKeys[i]]["isChanged"] = 0;
								}
								else{
									modObj[newKeys[i]]["isChanged"] = 1;
								}		
							}
							else{
								modObj[newKeys[i]]["isChanged"] = oldStudentObj[newKeys[i]] == newStudentObj[newKeys[i]] ? 0 : 1;
							}

							//some weird stuff with special circumstances
							if(newKeys[i] == "specialCircumstances" && newStudentObj[newKeys[i]] != null && newStudentObj[newKeys[i]] != ""){
								modObj[newKeys[i]]["isChanged"] = 1;
							}
							else if(newKeys[i] == "specialCircumstances"){
								modObj[newKeys[i]]["isChanged"] = 0;
							}
						}

						console.log(modObj);
						self.databaseInteractor.editStudent(modObj, function(err){
							if(!err){
								navigator.notification.alert("New student info saved!", function(){
									self.databaseInteractor.getSpecialCircumstances(newStudentObj.uid, function(err, specialCircumstances){
										$('#doneSpecialCircumstances').text(specialCircumstances);
										$('#specialCircumstances').val("");
									});
								}, "Students");
							}
						});
					}
					else{
    					navigator.notification.alert("There was an error. Please try again!", function(){}, "Students");	
					}
				});

			});

			//back button that leads to students page
			$('#editStudent').on("click", "#editStudentBackButtonStudents", function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer("change", "#students");
			});

			//back button that leades back to edit attendance page
			$('#editStudent').on("click", "#editStudentBackButtonAttendance", function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer("change", "#editAttendance", {reportUID: parseInt($(this).attr("href"))});
			});

			$('#editStudent').on("click", '#editStudentBackStudentToAttendance', function(event){
				event.preventDefault();
				$.mobile.pageContainer.pagecontainer("change", "#students", {reportUID: parseInt($(this).attr("href"))});
			});

			$('#students').on('click', "#newStudent", function(event){
				event.preventDefault();
				self.databaseInteractor.addNewStudent({isAdult: 0, isNew: 1}, function(err, uid){
					if(!err){
						event.preventDefault();
						$.mobile.pageContainer.pagecontainer('change', "#editStudent", {studentUID: uid});
					}
				});
			});

		},

		registerEvents: function(){
			$(document).on("pagebeforechange", function(event, data){
				if(data.toPage == "#home"){
					self.renderHomepage();
				}
				else if(data.toPage == "#students"){
					//if there is an report UID to the request
					if(data.options.reportUID){
						self.renderStudentsWithReport(data.options.reportUID);
					}
					else{
						self.renderStudents();
					}
				}
				else if(data.toPage == "#editAttendance"){
					//if there is report UID with the request
					if(data.options.reportUID){
						self.renderEditAttendance(data.options.reportUID);
					}
					else{
						self.renderNewEditAttendance();
					}
				}
				else if(data.toPage == "#editStudent"){
					var currentPage = $.mobile.pageContainer.pagecontainer("getActivePage").context.location.hash;

					
					if(currentPage == '#students' && $('#renderedWithReport').text() == "true"){
						currentPage = currentPage + "WithReport";
					}

					self.renderEditStudentPage(data.options.studentUID, currentPage);
				}

			});
		},

		//adds the active attendance reports to the homepage
		renderHomepage: function(){
			console.log("rendering home page!");

			//find the tbody element and empty it
			var tbody = $('#attendanceReports > tbody');
			tbody.empty();
			self.databaseInteractor.getReports(function(err, reports){
				if(!err){
					if(reports.length > 0){
						//loop through the reports
						$.each(reports, function(i, value){
							//get what classroom the report was
							self.databaseInteractor.getClassroom(reports[i].classroomUID, function(err, classroom){
								if(!err){
									//get what class type the report was
									self.databaseInteractor.getClassType(reports[i].classTypeUID, function(err, classType){
										if(!err){
											//create a row
											var row = $("<tr></tr>");

											//add the columns to the rows
											row.append('<td>' + classroom.name + '</td>');
											row.append('<td>' + classType.name + '</td>');
											row.append('<td>' + reports[i].whenRecorded.substring(11, 19) + '</td>');
											row.append('<td><a href="' + reports[i].uid + '" class="edit-attendance-button ui-btn ui-shadow ui-corner-all ui-icon-edit ui-btn-icon-notext">Edit</a></td>');

											//add the row to the body of the table
											tbody.append(row);
										}
									});
								}
							});
						});
					}
				}
			});
		},

		renderEditStudentPage: function(uid, prevPage){
			console.log("rendering edit student page!");
			console.log(prevPage);
			$('#editStudentHeader').remove('.backEdit');

			//make the appropiate backbutton lead to the appropriate place
			if(prevPage == '#students'){
				$('#editStudentHeader').append('<a href="#" class="backEdit ui-btn-left ui-btn ui-icon-back ui-btn-icon-notext ui-shadow ui-corner-all"  id="editStudentBackButtonStudents">Back</a>');
			}
			else if(prevPage == "#studentsWithReport"){
				$('#editStudentHeader').append('<a href="' + $("#reportUID").text() + '" class="backEdit ui-btn-left ui-btn ui-icon-back ui-btn-icon-notext ui-shadow ui-corner-all"  id="editStudentBackStudentToAttendance">Back</a>')
			}
			else if(prevPage == '#editAttendance&ui-state=dialog'){
				$('#editStudentHeader').append('<a href="' + $("#reportUID").text() + '" class="backEdit ui-btn-left ui-btn ui-icon-back ui-btn-icon-notext ui-shadow ui-corner-all"  id="editStudentBackButtonAttendance">Back</a>');
			}

			// search studenttable in the database for the specified uid and returns the matched student as a student object
			self.databaseInteractor.getStudent(uid, function(err, studentObj){
				if(!err){
					// insert data from database about the student to the page
					$('#uid').val(studentObj.uid);
					$('#name').val(studentObj.name);
					$('#isAdult').val(studentObj.isAdult).attr('selected', true).siblings('option').removeAttr('selected');
					$('#parent1').val(studentObj.parent1);
					$('#parent2').val(studentObj.parent2);
					$('#address').val(studentObj.address);
					$('#phone').val(studentObj.phone);
					$('#doneSpecialCircumstances').text(studentObj.specialCircumstances ? studentObj.specialCircumstances : "");
					$('#specialCircumstances').val("");
					$('#uniformSize').val(studentObj.uniformSize);
					// $('#isNew').val(studentObj.isNew);
				}
				else{
					console.log("Error matching student uid!");
				}
			});
		},

		//render students with a specific report
		renderStudentsWithReport: function(uid){
			console.log("rendering students page with uid.");
			$('#renderedWithReport').text("true");

			$("#studentsBody").append('<p hidden id=studentReportUID>' + uid +'</p>');
			//get the list element on the students page
			var ul = $("#studentsList");
			ul.empty();

			self.databaseInteractor.getStudents(function(err, students){
				if(!err){
					$.each(students, function(i, value){
						ul.append('<li data-icon="plus"><a class="addThisStudentToReport" href="' + students[i].uid + '">' + students[i].name + '</a></li>');
					});

					ul.listview( "refresh" );
				}
			});

		},

		renderStudents: function(){
			console.log("rendering students page.");
			$('#renderedWithReport').text("false");

			//get the list element on the students page
			var ul = $("#studentsList");
			ul.empty();

			self.databaseInteractor.getStudents(function(err, students){
				if(!err){
					$.each(students, function(i, value){
						ul.append('<li><a class="editStudentButton" href="' + students[i].uid + '">' + students[i].name + '</li>');
					});

					ul.listview( "refresh" );
				}
			});

		},

		renderNewEditAttendance: function(){
			self.databaseInteractor.createReport([], 1, 1, 1, function(err, reportUID){
				if(!err){
					self.renderEditAttendance(reportUID);
				}
			});
		},

		renderEditAttendance: function(uid){
			console.log("rendering attendance page!");
			$('#reportUID').text(uid);
			self.databaseInteractor.getReport(uid, function(err, report){
				console.log(report);
				if(!err){
					//add the attendance fields to the select tags
					self.addAttendanceFields(function(err){
						if(!err){
							//set the values of the selects to the current values.
							$('#classrooms').val(report.classroomUID).selectmenu('refresh');
							$('#classtypes').val(report.classTypeUID).selectmenu('refresh');
							$('#snacks').val(report.snackUID).selectmenu('refresh');
							$('#teachers').val(report.teachers).selectmenu('refresh');
							$('#reportComments').val(report.comments);

							//find the tbody element
							var tbody = $('#editAttendanceTable > tbody');
							tbody.empty();

							$.each(report.individualRecords, function(i, value){
								//create a row
								var tr = $('<tr id="recordUID' + report.individualRecords[i].uid + '"></tr>');

								//append the tds to the rows

								//name field
								tr.append('<td>' + report.individualRecords[i].name + '</td>');

								//Pts. field
								tr.append('<td> <a href="#addDisciplinePoint' + report.individualRecords[i].uid+ '" class="addDisciplinePoint ui-btn ui-shadow ui-corner-all ui-icon-plus ui-btn-icon-right">' + report.individualRecords[i].disciplinePoints + '</a></td>');

								//ate field
								var ateTd = $('<td></td>');
								var ateForm = $('<form></form>');
								var ate = $('<input type="checkbox" id="' + report.individualRecords[i].uid  + '" name="ate" class="ate" data-mini="true"/>')
								// var ate = $('<select id="' + report.individualRecords[i].uid  + '" name="ate" class="ate" data-role="slider"> <option value="0">No</option> <option value="1">Yes</option> </select>');
								if(report.individualRecords[i].didEat == 1){
									ate.prop("checked", true);
								}
								else{
									ate.prop("checked", false);
								}
								ateForm.append(ate);
								ateTd.append(ateForm);
								tr.append(ateTd);

								//other button field
									var otherTd = $('<td></td>');
									otherTd.append('<a href="#otherPopup' + report.individualRecords[i].uid + '" data-rel="popup" data-transition="slideup" class="ui-btn ui-shadow ui-corner-all ui-icon-bars ui-btn-icon-notext">Other</a>');

									//create the popup
									var popupDiv = $('<div data-role="popup" id="otherPopup' + report.individualRecords[i].uid + '"></div>');
									var popupForm = $('<form></form>');
									var popupUl = $('<ul data-role="listview" style="min-width:210px;"></ul>');

									//put in other a header
									popupUl.append('<li data-role="list-divider">Other Fields</li>');

									//delete field
									popupUl.append('<li><div class="ui-field-contain"><label for="deleteStudentFromReport">Delete:</label><a href="' + report.individualRecords[i].uid + '" class="deleteStudentFromReport ui-btn ui-shadow ui-corner-all ui-icon-minus ui-btn-icon-notext"> Delete </a></div></li>');

									//put in the edit button
									popupUl.append('<li><div class="ui-field-contain"><label for="editStudentFromReport">Edit:</label><a href="' + report.individualRecords[i].studentUID + '" class="editStudentFromReport ui-btn ui-shadow ui-corner-all ui-icon-edit ui-btn-icon-notext">Edit</a></div></li>');

									//put in the comments field
									popupUl.append('<li><div class="ui-field-contain"><label for="comments">Comments:</label> <textarea name="comments" class="comments" id="comments' + report.individualRecords[i].uid + '">' + report.individualRecords[i].comments + '</textarea> <a href="' + report.individualRecords[i].uid + '" class="saveComment ui-btn ui-shadow ui-corner-all">Save</a></div></li>');
									
									//combine everything and put it on the page.
									popupForm.append(popupUl);
									popupDiv.append(popupForm);
									otherTd.append(popupDiv);

								//actualy add it to the tr
								tr.append(otherTd);

								//add the tr to the tbody
								tbody.append(tr);
							});
							// console.log($('#editAttendanceTable').html());
							$('#editAttendanceTable').table('refresh');
							$('#editAttendanceTable').enhanceWithin();
						}
					});
				}
			});
		},

		addAttendanceFields: function(callback){
			//get the classrooms
			self.databaseInteractor.getClassrooms(function(err, classrooms){
				if(!err){
					//add the classrooms to the select
					var classroomsSelect = $('#classrooms');
					classroomsSelect.empty();
					$.each(classrooms, function(i, value){
						classroomsSelect.append('<option value="'+ classrooms[i].uid + '">' + classrooms[i].name + '</option>');
					});

					//get the class types
					self.databaseInteractor.getClassTypes(function(err, classTypes){
						if(!err){

							//add the class types to the select
							var classTypesSelect = $('#classtypes');
							classTypesSelect.empty();
							$.each(classTypes, function(i, value){
								classTypesSelect.append('<option value="' + classTypes[i].uid + '">' + classTypes[i].name + '</option>');
							});
							//get the snacks
							self.databaseInteractor.getSnacks(function(err, snacks){
								if(!err){
									//add the snacks to the select
									var snacksSelect = $('#snacks');
									snacksSelect.empty();
									$.each(snacks, function(i, value){
										snacksSelect.append('<option value="' + snacks[i].uid + '">' + snacks[i].name + '</option>');
									});

									//get the teachers
									self.databaseInteractor.getTeachers(function(err, teachers){
										if(!err){
											//add the teachers to the select
											var teachersSelect = $('#teachers');
											teachersSelect.empty();
											$.each(teachers, function(i, value){
												teachersSelect.append('<option value="' + teachers[i].uid + '">' + teachers[i].name + '</option>');
											});

											teachersSelect.selectmenu("refresh");
											callback(false); //there was not an error
										}
										else{
											callback(true); //there was an error
										}
									});
								}
								else{
									callback(true); //there was an error
								}
							});
						}
						else{
							callback(true); //there was an error
						}
					});
				}
				else{
					callback(true); //there was an error
				}
			});
		},

		//the login procedure to follow after the button is clicked.
		buttonLogin: function(){
			login(self.databaseInteractor, function(err){
	            if(!err){
	                //use the app!
	                self.renderHomepage();
	             }
	             else{
	                self.buttonLogin();
	            }
        	});
		}
	};

	controller.initialize();
	return controller;
}
