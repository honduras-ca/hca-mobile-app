function login(databaseInteractor, callback){
	console.log("You telling me to login");
	//I assume that the database interactor has only done a preliminary initialisation

	//find out how the device is connected to the internet.
	var networkState = navigator.connection.type;

	databaseInteractor.selectSQL("SELECT * FROM authenticated;", [], function(err, res){
		if(!err){
			//if a user is currently "logged in".
			if(res.rows.length > 0){
				//it is assumed that if they logged in once the database interactor is fully initialized
				console.log("The user has successfully logged in");
				callback(false); //there wasn't an error and the user is now logged in.
			}
			else{ //there is no user logged-in
				//if they are not connected to the internet
				if(networkState == Connection.NONE || networkState == Connection.UNKNOWN){
					navigator.notification.alert("Please connect to the internet and try again.", function(){
						callback(true); //if they are not connected to the internet
					}, "Login");
				}
				else{ //if they are connected to the internet
					window.plugins.googleplus.login({
						'scopes': 'profile email', // optional - space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
	            		'webClientId': '191929119761-0e7547rogrf1pjhmkssb583570nl3cf1.apps.googleusercontent.com', // optional - clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
	            		'offline': true // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
					}, function(obj){
						//send the data to the server and see if they are an authenticated user.
						console.log("Google log-in succeded!");
						var postObject = {accessId: obj.idToken, email: obj.email};
	       				cordova.plugin.http.setDataSerializer('json');
						cordova.plugin.http.sendRequest("https://hca.thelounge.sx/authMobile", {method:'post', data: postObject, headers: {}}, function(res){
							
							//parse the response
							res = JSON.parse(res.data);

							//if the server decides that the user is authenticated
							if(res.isAuthenticated == 1){
								//add the new user to the authenticated table
								databaseInteractor.insertSQL("INSERT INTO authenticated (email, accessId) VALUES (?, ?);", [obj.email, obj.idToken], function(err){
									if(!err){
										//this is a new user, so wipe the database and then sync with the web side.
										databaseInteractor.fullInitialize(function(err){
											if(!err){
												//sync
												databaseInteractor.sync(function(err){
													if(!err){
														console.log("The user is logged in, and the database is ready to use.");
														callback(false); //there wasn't an error and the user is now logged in.
													}
													else{
														//forceably log out the user
														logout(true, databaseInteractor, function(err){
															if(!err){
																console.log("Login database sync failed");
															}	
															else{
																console.log("Log out error at database sync");
															}
															callback(true); //there was an error and the user is not logged in.
														});
													}
												});
											}
											else{
												//forceably log out the user
												logout(true, databaseInteractor, function(err){
													if(!err){
														console.log("Login database initialization failed");
													}	
													else{
														console.log("Log out error at database initialization");
													}
													callback(true); //there was an error and the user is not logged in.
												});
											}
										});
									}
									else{
										//forceably log out the user
										logout(true, databaseInteractor, function(err){
											if(!err){
												console.log("Database insert of new user failed!");
											}
											else{
												console.log("Log out error at database insert");
											}
											callback(true); //there was an error and the user is not logged in.
										});
									}
								});
							}
							else{ //if the user isn't authenticated
								//forceably log out the user
								logout(true, databaseInteractor, function(err){
									if(!err){
										console.log("You are not an authenticated user please login as a different email.");
										navigator.notification.alert("You are not an authenticated user please login as a different email.", function(){
											callback(true);
										}, "Login");
									}
									else{
										console.log("Log out error at line 86");
									}
									callback(true); //there was an error and the user is not logged in.
								});
							}
						}, function(err){ //if there was an error with the post request
							//forceable log the user out
							logout(true, databaseInteractor, function(err2){
								if(!err2){
									console.log("Could not authenticate user with server ERROR: " + err.err);		
								}
								else{
									console.log("Log out error at line 100!");
								}
								callback(true);  //there was an error and the user is not logged in.
							});	
						});
					//if there was an error with google sign-in
					}, function(message){
						console.log("Google sign in error: " + message);
						callback(true);  //there was an error and the user is not logged in.
					});
				}
			}
		}
		else{
			console.log("There was an error with the selection from authenticated table.");
			callback(true); //there was an error and the user is not logged in.
		}
	});
}


//log out the user
function logout(forced, databaseInteractor, callback){
	console.log("you telling me to logout!");
	//find out how the device is connected to the internet.
	var networkState = navigator.connection.type;

	//if the user isn't connected to the internet, they can't log out
	if(networkState == Connection.NONE || networkState == Connection.UNKNOWN){
		navigator.notification.alert("Please connect to the internet and try again.", function(){
			callback(true); //if they are not connected to the internet then they cannot log out.
		}, "Logout");
	}
	else{ //if the user is connected to the internet
		//if forced is false
		if(forced == false){
			//if the user says they are sure they want to log out
			navigator.notification.confirm("Are you sure you want to log out. All un-synced data will be lost!", function(res){
				if(res == 1){
					databaseInteractor.insertSQL("DELETE FROM authenticated;", [], function(err){
						if(!err){
							//try logging out
							window.plugins.googleplus.logout(function(message){
								console.log("Logout successful: " + message);
								window.plugins.googleplus.disconnect(function(msg) {
	      							console.log("Disconnect successful: " + msg);
	      							callback(false); //there was not an error and the user is logged out.
	  							});
							});
						}
						else{
							console.log("Deletion from authenticated table failed!");
							callback(true); //there was an error and the user is not logged out.
						}
					});
				}
				
				else{ //if the user decides they don't want to log out.
					callback(true); //there was an error and the user is not logged out.
				}

			}, 'Logout', ["Yes", "No"]);
		}
		else{ //if forced is true
			databaseInteractor.insertSQL("DELETE FROM authenticated;", [], function(err){
					if(!err){
						window.plugins.googleplus.logout(function(message){
							console.log("Logout successful: " + message);
							window.plugins.googleplus.disconnect(function(msg) {
      							console.log("Disconnect successful: " + msg);
      							callback(false); //there was not an error and the user is logged out.
  							});
						});
					}
					else{
						console.log("Deletion from authenticated table failed!");
						callback(true); //there was an error and the user is not logged out.
					}
			});
		}

	}
}