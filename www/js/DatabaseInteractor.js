var DatabaseInteractor = function(callback) { //the callback function takes an err and a databaseInteractor object.
	console.log("You told me to make a DB interactor.");

	var db = window.sqlitePlugin.openDatabase({
		name: 'local.db', 
		location:'default',
		androidDatabaseProvider: 'system'
	});

	var databaseInteractor = {
		preliminaryInitialize: function(callback){
			db.transaction(function(tx){
				//who is the person logged into mobile
				tx.executeSql('CREATE TABLE IF NOT EXISTS authenticated (uid INTEGER PRIMARY KEY AUTOINCREMENT, email VARCHAR(32), accessId VARCHAR(200));');
			}, function(error){
				console.log("Preliminary initialisation ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Preliminary initialisation succeded!");
				callback(false); //there was not an error
			});
		},

		fullInitialize: function(callback){
			db.transaction(function(tx){
				tx.executeSql('DROP TABLE IF EXISTS classrooms;');
				tx.executeSql('DROP TABLE IF EXISTS classTypes;');
				tx.executeSql('DROP TABLE IF EXISTS snacks;');
				tx.executeSql('DROP TABLE IF EXISTS students;');
				tx.executeSql('DROP TABLE IF EXISTS studentMods;');
				tx.executeSql('DROP TABLE IF EXISTS attendanceReports;');
				tx.executeSql('DROP TABLE IF EXISTS individualRecords;');
				tx.executeSql('DROP TABLE IF EXISTS users;');
				tx.executeSql('DROP TABLE IF EXISTS teacherReportLink;');

				//classroom location names
				tx.executeSql('CREATE TABLE IF NOT EXISTS classrooms (uid INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(32));');
				
				//class types (time/level)
				tx.executeSql('CREATE TABLE IF NOT EXISTS classTypes (uid INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(32), isAdult TINYINT(1));');
				
				//table of known snack types
				tx.executeSql('CREATE TABLE IF NOT EXISTS snacks (uid INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(64));');
				
				//all HCA student info
				tx.executeSql('CREATE TABLE IF NOT EXISTS students (uid INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(150), isAdult TINYINT(1), parent1 VARCHAR(150), parent2 VARCHAR(150), address VARCHAR(100) DEFAULT NULL, phone VARCHAR(16) DEFAULT NULL, specialCircumstances TEXT DEFAULT NULL, uniformSize VARCHAR(16) DEFAULT NULL, isNew TINYINT(1) DEFAULT 0);');

				//tracks modifications in local student profiles (so we can isolate what was modified)
				tx.executeSql('CREATE TABLE IF NOT EXISTS studentMods (uid INTEGER PRIMARY KEY AUTOINCREMENT, studentUID INTEGER, nameChange TINYINT(1) DEFAULT 0, parent1Change TINYINT(1) DEFAULT 0, parent2Change TINYINT(1) DEFAULT 0, isAdultChange TINYINT(1) DEFAULT 0, addressChange TINYINT(1) DEFAULT 0, phoneChange TINYINT(1) DEFAULT 0, specialCircumstancesChange TINYINT(1) DEFAULT 0, uniformSizeChange TINYINT(1) DEFAULT 0, FOREIGN KEY (studentUID) REFERENCES students(uid));');
				
				//all attendance reports recorded on-site by teachers
				tx.executeSql('CREATE TABLE IF NOT EXISTS attendanceReports (uid INTEGER PRIMARY KEY AUTOINCREMENT, classroomUID INTEGER, classTypeUID INTEGER, snackUID INTEGER, whenRecorded DATETIME, comments TEXT, FOREIGN KEY (classroomUID) REFERENCES classrooms(uid), FOREIGN KEY (classTypeUID) REFERENCES classTypes(uid), FOREIGN KEY (snackUID) REFERENCES snacks(uid));');
				
				//all individual records that make up attendance reports
				tx.executeSql('CREATE TABLE IF NOT EXISTS individualRecords (uid INTEGER PRIMARY KEY AUTOINCREMENT, reportUID INTEGER, studentUID INTEGER, didEat TINYINT(1), disciplinePoints INTEGER DEFAULT 0, comments TEXT, FOREIGN KEY (studentUID) REFERENCES students(uid), FOREIGN KEY (reportUID) REFERENCES attendanceReports(uid));');
				
				//users profile information
				tx.executeSql('CREATE TABLE IF NOT EXISTS users (uid INTEGER PRIMARY KEY AUTOINCREMENT, email VARCHAR(32), name VARCHAR(64), isAdmin TINYINT(1));');

				//teacher report link table
				tx.executeSql('CREATE TABLE IF NOT EXISTS teacherReportLink(uid INTEGER PRIMARY KEY AUTOINCREMENT, reportUID INTEGER, teacherUID INTEGER, FOREIGN KEY (reportUID) REFERENCES attendanceReports(uid), FOREIGN KEY (teacherUID) REFERENCES teachers(uid));');

				//insert an unknown value into some fields.
				// tx.executeSql('INSERT INTO classrooms (name) VALUES ("Unknown");');
				// tx.executeSql('INSERT INTO classTypes (name, isAdult) VALUES ("Unknown", NULL);');
				// tx.executeSql('INSERT INTO snacks (name) VALUES ("Unknown");');

	//-------------------------------------------------debug-----------------------------------------------------

	//			tx.executeSql('INSERT INTO teachers (email, name) values ("yo@gmail.com", "Jerry");');
    //             tx.executeSql('SELECT * FROM teachers', [], function(tx, res){ 
    //             	console.log(res.rows.item(0).email);
    //             });
    // 			tx.executeSql('INSERT INTO classrooms (name) VALUES ("PEP centro"), ("PEP-2"), ("PEP-3"), ("Volunteer House 1");');
    // 			tx.executeSql('INSERT INTO classtimes (timeString) VALUES ("AM"), ("PM");');
    // 			tx.executeSql('INSERT INTO snacks (name) VALUES ("Snack A"), ("Snack B");');
    			// tx.executeSql('INSERT INTO students (name, isAdult, parent1, parent2, specialCircumstances) VALUES ("Student A", 0, "Parent1 A", "Parent2 A", NULL),  ("Student B", 0, "Parent1 B", "Parent2 B", "Has a special circumstance."), ("Student C", 1, "Parent1 C", "Parent2 C", NULL), ("Student D", 1, "Parent1 D", "Parent2 D", NULL);');
    // 			tx.executeSql('INSERT INTO attendanceReports (classroomUID, classTypeUID, snackUID, whenRecorded, comments) VALUES (1, 1, 2, datetime("now"), "The class was rather rowdy today."), (1, 2, 2, datetime("now"), "The class behaved quite well."), (3, 1, 1, datetime("now"), "The class was okay.");');
    // 			tx.executeSql('INSERT INTO individualRecords (reportUID, studentUID, didEat, disciplinePoints, comments) VALUES (1, 2, 0, 3, "Student behaved poorly."), (1, 3, 1, 1, "Was rather okay in class."), (2, 1, 1, 0, "Was respectful.");');
    // 			tx.executeSql('INSERT INTO users (email, name, isAdmin) VALUES ("adminA@gmail.com", "Admin A"),("adminB@gmail.com", "Admin B");');
    // 			tx.executeSql('INSERT INTO teachers (email, name) VALUES ("teacherA@gmail.com", "Teacher A"), ("teacherB@gmail.com", "Teacher B"), ("teacherC@gmail.com", "Teacher C");');
    // 			tx.executeSql('INSERT INTO teacherReportLink (reportUID, teacherUID) VALUES (1, 1), (1, 2), (2, 1), (2, 2), (3, 3);');

    			// tx.executeSql('UPDATE students SET name = "modified1", specialCircumstances = "||New one||New2 one" WHERE uid = 1;')
    			// tx.executeSql('INSERT INTO studentMods (studentUID, nameChange, specialCircumstancesChange) VALUES (1, 1, 1);');

    // 			tx.executeSql('UPDATE students SET name = "modified2" WHERE uid = 2;')
    // 			tx.executeSql('INSERT INTO studentMods (studentUID, nameChange) VALUES (2, 1);');


			}, function(error) {
    			console.log('Full initialisation ERROR: ' + error.message);
    			callback(true); //there was an error
  			}, function() {
    			console.log('Full initialisation succeded!');
    			callback(false); //there was not an error
  			});
		},

		//deletes all information from the tables in the database.
		clear: function(callback){
			db.transaction(function(tx){
				
				//delete all information from all of the tables except authenticated
				tx.executeSql('DELETE FROM classrooms;');
				tx.executeSql('DELETE FROM classTypes;');
				tx.executeSql('DELETE FROM snacks;');
				tx.executeSql('DELETE FROM students;');
				tx.executeSql('DELETE FROM studentMods;');
				tx.executeSql('DELETE FROM attendanceReports;');
				tx.executeSql('DELETE FROM individualRecords;');
				tx.executeSql('DELETE FROM users;');
				tx.executeSql('DELETE FROM teacherReportLink;');

				//reset attendance report auto-increment to 0
				tx.executeSql('DELETE FROM sqlite_sequence WHERE name="attendanceReports";');

			}, function(error) {
				console.log("Clear ERROR: " + error.message);
				callback(true); //there was an error
			}, function() {
				console.log("Clear succeded!");
				callback(false); //there was no error
			});
		},

		sync: function(callback){
			var self = this;
			var postObject = {};
			db.transaction(function(tx){

				//gets all of the brand new students and adds them to the sync object.
				var newStudentList = [];
				tx.executeSql('SELECT uid, name, isAdult, parent1, parent2, address, phone, specialCircumstances, uniformSize FROM students WHERE isNew = 1;', [], function(tx, res){
					for(i = 0; i < res.rows.length; i++){
						var specialCircumstances = res.rows.item(i).specialCircumstances;
						if(specialCircumstances){
							specialCircumstances = specialCircumstances.split('||');
							if(specialCircumstances.length > 1){
								specialCircumstances = specialCircumstances.slice(1, specialCircumstances.length);
							}
							specialCircumstances = specialCircumstances.join(' ');
							res.rows.item(i).specialCircumstances = specialCircumstances;
						}
						newStudentList.push(res.rows.item(i));
					}
					postObject.newStudents = newStudentList;
				});

				//gets all of the modified students and adds them to the sync object.
				var modifiedStudentsList = [];
				tx.executeSql('SELECT * FROM studentMods;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						modifiedStudChanges = res.rows.item(i);
						tx.executeSql('SELECT * FROM students WHERE uid = ?', [modifiedStudChanges.studentUID], function(tx, res2){
							var modifiedStud = {};
							var modifiedData = res2.rows.item(0);

							//splits the special circumstanes across the new edits, and then gets rid of what was already there!
							var specialCircumstances = modifiedData.specialCircumstances;
							if(specialCircumstances){
								specialCircumstances = specialCircumstances.split('||');
								if(specialCircumstances.length > 1){
									specialCircumstances = specialCircumstances.slice(1, specialCircumstances.length);
								}
								specialCircumstances = specialCircumstances.join(' ');
							}
							modifiedData.specialCircumstances = specialCircumstances;

							modifiedStud.uid = modifiedData.uid //never changes.
							modifiedStud.name = {"value":modifiedData.name, "isChanged":modifiedStudChanges.nameChange};
							modifiedStud.parent1 = {"value":modifiedData.parent1, "isChanged":modifiedStudChanges.parent1Change}; 
							modifiedStud.parent2 = {"value":modifiedData.parent2, "isChanged":modifiedStudChanges.parent2Change};
							modifiedStud.isAdult = {"value":modifiedData.isAdult, "isChanged":modifiedStudChanges.isAdultChange};
							modifiedStud.address = {"value":modifiedData.address, "isChanged":modifiedStudChanges.addressChange};
							modifiedStud.phone = {"value":modifiedData.phone, "isChanged":modifiedStudChanges.phoneChange};
							modifiedStud.specialCircumstances = {"value":modifiedData.specialCircumstances, "isChanged":modifiedStudChanges.specialCircumstancesChange}; // This needs to be changed for Web Backend Syncing
							modifiedStud.uniformSize = {"value":modifiedData.uniformSize, "isChanged":modifiedStudChanges.uniformSizeChange};
							modifiedStudentsList.push(modifiedStud);
						});
					}
					postObject.modifiedStudents = modifiedStudentsList;
				});

				//gets the attendance reports and adds them to the sync object
				var attendanceReportsList = [];
				tx.executeSql('SELECT uid, classroomUID, classTypeUID, snackUID, whenRecorded, comments FROM attendanceReports;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						attendanceReportsList.push(res.rows.item(i));
					}
					postObject.attendanceReports = attendanceReportsList;			
				});

				//gets the individual records and adds them to the sync object
				var individualRecordsList = [];
				tx.executeSql('SELECT uid, reportUID, studentUID, didEat, disciplinePoints, comments FROM individualRecords;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						individualRecordsList.push(res.rows.item(i));
					}
					postObject.individualRecords = individualRecordsList;
				});

				//gets the tacher report link information and adds it to the sync object
				var teacherReportLinkList = [];
				tx.executeSql('SELECT reportUID, teacherUID FROM teacherReportLink;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						teacherReportLinkList.push(res.rows.item(i));
					}
					postObject.teacherReportLink = teacherReportLinkList;
				});

				//add the current user's accessId to the post request.
				tx.executeSql('SELECT accessId, email FROM authenticated;', [], function(tx, res){
					postObject.accessId = res.rows.item(0).accessId;
					postObject.email = res.rows.item(0).email;
				});

			}, function(error) {
				console.log('Sync object ERROR: ' + error.message);
				callback(true); //there was an error.
			}, function(){
				console.log('Sync object creation succeded!');
				console.log(postObject);
				cordova.plugin.http.setDataSerializer('json');
				cordova.plugin.http.sendRequest("https://hca.thelounge.sx/sync", {method:'post', data: postObject, headers: {}} , function(res){
					res = JSON.parse(res.data);
					res = res.data;
					self.syncResponse(res, function(err){
						if(!err){
							callback(false); //there was not an error.
						}
						else{
							callback(true); //there was an error.
						}
					});
				}, function(err){
					console.log(err.status);
					console.log(err.error);
				});
			});
		},

		//takes the servers response to the sync and clears the database and then replaces it with the data from the servers response
		syncResponse: function(serverRes, callback){
			this.clear(function(err){
				if(!err){
					var batchInsert = [];

					//puts in the classroom information
					for(var i = 0; i < serverRes.classrooms.length; i++){
						var classroom = serverRes.classrooms[i];
						batchInsert.push(["INSERT INTO classrooms (uid, name) VALUES (?, ?);", [classroom.uid, classroom.name]]);
					}

					//puts in the classType information
					for(var i = 0; i < serverRes.classTypes.length; i++){
						var classType = serverRes.classTypes[i];
						batchInsert.push(["INSERT INTO classTypes (uid, name, isAdult) VALUES (?, ?,  ?);", [classType.uid, classType.name, classType.isAdult]]);
					}

					//puts in the snack information
					for(var i = 0; i < serverRes.snacks.length; i++){
						var snack = serverRes.snacks[i];
						batchInsert.push(["INSERT INTO snacks (uid, name) VALUES (?, ?);", [snack.uid, snack.name]]);
					}


					//puts in the student information
					for(var i = 0; i < serverRes.students.length; i++){
						var student = serverRes.students[i];
						batchInsert.push(["INSERT INTO students (uid, name, isAdult, parent1, parent2, address, phone, specialCircumstances, uniformSize) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);", [student.uid, student.name, student.isAdult, student.parent1, student.parent2, student.address, student.phone, student.specialCircumstances, student.uniformSize]]);
					}

					//puts in all of the users information
	
					for(var i = 0; i < serverRes.users.length; i++){
						user = serverRes.users[i];
						batchInsert.push(["INSERT INTO users (uid, email, name, isAdmin) VALUES (?, ?, ?, ?);", [user.uid, user.email, user.name, user.isAdmin]]);
					}

					db.sqlBatch(batchInsert,
						function(){
							console.log("Sync response insert succeded!");
							callback(false); //there was not an error
						},
						function(error){
							console.log("Synce response insert ERROR: " + error.message);
							callback(true); //there was an error
						});
				}
				else{
					callback(true); //there was an error.
				}	
			});			
		},

		//execute an abribtray insert SQL command
		insertSQL: function(command, values, callback){
			db.transaction(function(tx){
				tx.executeSql(command, values);
			}, function(error){
				console.log("Insert SQL ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Insert SQL succeded!");
				callback(false); //there was not an error
			});
		},

		//execute an arbitrary select SQL command
		selectSQL: function(command, values, callback){
			var response = null;
			db.transaction(function(tx){
				tx.executeSql(command, values, function(tx, res){
					response = res;
				});
			}, function(error){
				console.log("Select SQL ERROR: " + error.message);
				callback(true, null); //there was an error
			}, function(){
				console.log("Select SQL succeded!");
				callback(false, response); //there was not an error
			});
		},

		//gets a list of every student in the database.
		getStudents: function(callback){
			var students = [];
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM students;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						//if there are special circumstances, get rid of the special characters
						if(res.rows.item(i).specialCircumstances){
							res.rows.item(i).specialCircumstances = res.rows.item(i).specialCircumstances.split('||').join(' ');
						}
						students.push(res.rows.item(i));
					}
				});
			}, function(error){
				console.log("Getting students ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no students to return.
			}, function(){
				console.log("Getting students succeded!");
				callback(false, students); //there was not an error, and so here are the students.
			});
		},

		getStudent: function(uid, callback){
			student = {};
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM students WHERE uid = ?;', [uid], function(tx, res){
						//if there are special circumstances, get rid of the special characters
						console.log(res.rows.item(0).specialCircumstances);
						if(res.rows.item(0).specialCircumstances != null){
							res.rows.item(0).specialCircumstances = res.rows.item(0).specialCircumstances.split('||').join(' ');
						}
						student = res.rows.item(0);
				});
			}, function(error){
				console.log("Getting student ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no students to return.
			}, function(){
				console.log("Getting student succeded!");
				callback(false, student); //there was not an error, and so here are the students.
			});
		},

		editStudent(modObj, callback){
			var self = this;
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM studentMods WHERE studentUID = ?;', [modObj.uid.value], function(tx, modRes){
					tx.executeSql('UPDATE students SET name=?, isAdult=?, parent1=?, parent2=?, address=?, phone=?, uniformSize=? WHERE uid = ?;', [modObj.name.value, modObj.isAdult.value, modObj.parent1.value, modObj.parent2.value, modObj.address.value, modObj.phone.value, modObj.uniformSize.value, modObj.uid.value], function(tx, updateStudRes){
						self.addSpecialCircumstance(modObj.uid.value, modObj.specialCircumstances, function(err){
							tx.executeSql('SELECT isNew FROM students WHERE uid = ?;', [modObj.uid.value], function(tx, isNewRes){
								if(isNewRes.rows.item(0).isNew != 1){
									if(modRes.rows.length > 0){
										//keeps the modObj from resetting each time you save
										var keys = Object.keys(modRes.rows.item(0));
										for(var i = 0; i<keys.length; i++){
											if(keys[i] != "studentUID" && modRes.rows.item(0)[keys[i]] == 1){
												modObj[keys[i].slice(0, keys[i].length - 6)].isChanged = 1;
											}
										}
										tx.executeSql('UPDATE studentMods SET nameChange=?, isAdultChange=?, parent1Change=?, parent2Change=?, addressChange=?, phoneChange=?, specialCircumstancesChange=?, uniformSizeChange=? WHERE studentUID=?;', [modObj.name.isChanged, modObj.isAdult.isChanged, modObj.parent1.isChanged, modObj.parent2.isChanged, modObj.address.isChanged, modObj.phone.isChanged, modObj.specialCircumstances.isChanged, modObj.uniformSize.isChanged, modObj.uid.value]);
									}
									else{
										tx.executeSql('INSERT INTO studentMods (studentUID, nameChange, isAdultChange, parent1Change, parent2Change, addressChange, phoneChange, specialCircumstancesChange, uniformSizeChange) VALUES (?,?,?,?,?,?,?,?,?);', [modObj.uid.value, modObj.name.isChanged, modObj.isAdult.isChanged, modObj.parent1.isChanged, modObj.parent2.isChanged, modObj.address.isChanged, modObj.phone.isChanged, modObj.specialCircumstances.isChanged, modObj.uniformSize.isChanged]);
									}
								}
							});
						});
					});
				});
 				
			}, function(error){
				console.log("save student ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("save student success!");
				callback(false); //there was not an error
			});
		},

		//gets the all classroom information
		getClassrooms: function(callback){
			var classrooms = [];
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM classrooms;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						classrooms.push(res.rows.item(i));
					}
				});
			}, function(error){
				console.log("Getting classrooms ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no classrooms to return.
			}, function(){
				console.log("Getting classrooms succeded!");
				callback(false, classrooms); //there was not an error, and so here are the classrooms.
			});
		},

		//gets a specific classroom
		getClassroom: function(uid, callback){
			var classroom = null;
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM classrooms WHERE uid = ?;', [uid], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						classroom = res.rows.item(i);
					}
				});
			}, function(error){
				console.log("Getting classroom ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no classrooms to return.
			}, function(){
				console.log("Getting classroom succeded!");
				callback(false, classroom); //there was not an error, and so here are the classrooms.
			});
		},

		setClassroom: function(reportUID, classroomUID, callback){
			db.transaction(function(tx){
				tx.executeSql('UPDATE attendanceReports SET classroomUID = ? WHERE uid = ?;', [classroomUID, reportUID]);
			}, function(error){
				console.log("Setting classroom ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Setting classroom succeded!");
				callback(false); //there was not an error
			});
		},

		//gets all of the classtimes information
		getClassTypes: function(callback){
			var classTypes = [];
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM classTypes;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						classTypes.push(res.rows.item(i));
					}
				});
			}, function(error){
				console.log("Getting classTypes ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no classTypes to return.
			}, function(){
				console.log("Getting classTypes succeded!");
				callback(false, classTypes); //there was not an error, and so here are the classTypes.
			});
		},

		//gets a specific classType information
		getClassType: function(uid, callback){
			var classType = null;
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM classTypes WHERE uid = ?;', [uid], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						classType = res.rows.item(i);
					}
				});
			}, function(error){
				console.log("Getting classType ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no classTypes to return.
			}, function(){
				console.log("Getting classType succeded!");
				callback(false, classType); //there was not an error, and so here are the classTypes.
			});
		},

		setClassType: function(reportUID, classTypeUID, callback){
			db.transaction(function(tx){
				tx.executeSql('UPDATE attendanceReports SET classTypeUID = ? WHERE uid = ?;', [classTypeUID, reportUID]);
			}, function(error){
				console.log("Setting classType ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Setting classType succeded!");
				callback(false); //there was not an error
			});
		},

		//gets all of the snack information
		getSnacks: function(callback){
			var snacks = [];
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM snacks;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						snacks.push(res.rows.item(i));
					}
				});
			}, function(error){
				console.log("Getting snacks ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no snacks to return.
			}, function(){
				console.log("Getting snacks succeded!");
				callback(false, snacks); //there was not an error, and so here are the snacks.
			});
		},

		//set the snack of a specific report
		setSnack: function(reportUID, snackUID, callback){
			db.transaction(function(tx){
				tx.executeSql('UPDATE attendanceReports SET snackUID = ? WHERE uid = ?;', [snackUID, reportUID]);
			}, function(error){
				console.log("Setting snack ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Setting snack succeded!");
				callback(false); //there was not an error
			});
		},

		//gets all of the teachers information
		getTeachers: function(callback){
			var teachers = [];
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM users WHERE isAdmin = 0;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						teachers.push(res.rows.item(i));
					}
				});
			}, function(error){
				console.log("Getting teachers ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no teachers to return.
			}, function(){
				console.log("Getting teachers succeded!");
				callback(false, teachers);
			});
		},

		setTeachers: function(reportUID, teachers, callback){
			db.transaction(function(tx){
				tx.executeSql('DELETE FROM teacherReportLink WHERE reportUID = ?;', [reportUID], function(tx, res){
					$.each(teachers, function(i, value){
						tx.executeSql('INSERT INTO teacherReportLink (reportUID, teacherUID) VALUES (?, ?);', [reportUID, teachers[i]]);
					});
				});
			}, function(error){
				console.log("Setting teachers ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Setting teachers succeded!");
				callback(false); //there was not an error
			});
		},

		//inserts a new student into the dastabase.
		addNewStudent: function(studentObj, callback){ //var stubenfObject = {name: "joe", parent1: "bob", parent2: "kate", isAdult: 0 is false 1 is true, address: NULL, phone: NULL, specialCircumstances: NULL, uniformSize: NULL}
			var studentUID = null;
 			db.transaction(function(tx){
 				tx.executeSql('INSERT INTO students (name, isAdult, parent1, parent2, address, phone, specialCircumstances, uniformSize, isNew) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);', [studentObj.name, studentObj.isAdult, studentObj.parent1, studentObj.parent2, studentObj.address, studentObj.phone, studentObj.specialCircumstances, studentObj.uniformSize, studentObj.isNew], function(tx, res){
 					tx.executeSql('SELECT MAX(uid) FROM students;', [], function(tx, res){
 						studentUID = res.rows.item(0)["MAX(uid)"];
 					});
 				}); 
			}, function(error){
				console.log("Insert student ERROR: " + error.message);
				callback(true, null); //there was an error
			}, function(){
				console.log("Insert student success!");
				callback(false, studentUID); //there was not an error
			});

		},

		//creates an attendance report, and returns the uid of that report.
		//also creates a report link between the report and the teacher who reported it.
		createReport: function(teacherUIDs, classroomUID, classTypeUID, snackUID, callback){
			var attendanceReportUID = null; 
			db.transaction(function(tx){
				tx.executeSql('INSERT INTO attendanceReports (classroomUID, classTypeUID, snackUID, whenRecorded) VALUES (?, ?, ?, datetime("now", "localtime"));', [classroomUID, classTypeUID, snackUID]);
				tx.executeSql('SELECT MAX(uid) FROM attendanceReports;', [], function(tx, res){
					attendanceReportUID = res.rows.item(0)["MAX(uid)"];
					for(var i = 0; i < teacherUIDs.length; i++){
						tx.executeSql('INSERT INTO teacherReportLink VALUES (?,?);', [attendanceReportUID, teacherUIDs[i]]);
					}		
				});
			}, function(error){
				console.log("Attendance report creation ERROR: " + error.message);
				callback(true, null); //there was an error creating the report, and so there is no uid to return.
			}, function(){
				console.log("Attendance report creation success!");
				callback(false, attendanceReportUID);
			});
		},

		getReports: function(callback){
			var reports = [];
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM attendanceReports;', [], function(tx, res){
					for(var i = 0; i < res.rows.length; i++){
						reports.push(res.rows.item(i));
					}
				});
			}, function(error){
				console.log("Getting reports ERROR: " + error.message);
				callback(true, null); //there was an error and so there are no reports
			}, function(){
				console.log("Getting reports succeded!");
				callback(false, reports); //there wasn't an error and so here are the reports
			});
		},

		getReport: function(uid, callback){
			var report = {};
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM attendanceReports WHERE uid = ?;', [uid], function(tx, res){
					if(res.rows.length > 0){
						report = res.rows.item(0);
					}
					tx.executeSql('SELECT * FROM teacherReportLink WHERE reportUID = ?;', [uid], function(tx, res){
						report.teachers = [];
						for(var i = 0; i < res.rows.length; i++){
							report.teachers.push(res.rows.item(i).teacherUID);
						}
						tx.executeSql('SELECT * FROM individualRecords WHERE reportUID = ?;', [uid], function(tx, res){
							report.individualRecords = [];
							for(var i = 0; i < res.rows.length; i++){
								report.individualRecords.push(res.rows.item(i));
								if(report.individualRecords[i].comments == null){
									report.individualRecords[i].comments = "";
								}
							}

							$.each(report.individualRecords, function(i, value){
								tx.executeSql('SELECT name FROM students WHERE uid = ?;', [report.individualRecords[i].studentUID], function(tx, res){
									report.individualRecords[i].name = res.rows.item(0).name;
								});
							});
						});
					});
				});
			}, function(error){
				console.log("Getting report ERROR: " + error.message);
				callback(true, null); //there was an error and so there is no report
			}, function(){
				console.log("Getting report succeded!");
				callback(false, report); //there wasn't an error and so here is the report
			});
		},

		setReportComments: function(reportUID, comment, callback){
			db.transaction(function(tx){
				tx.executeSql('UPDATE attendanceReports SET comments = ? WHERE uid = ?;', [comment, reportUID]);
			}, function(error){
				console.log("Setting report comments ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Setting report comments succeded!");
				callback(false); //there was not an error
			});
		},

		//search for students to add to report by their names.
		//returns a list of student objects with names containing input string.
		//search is case-insensitive.
		searchStudents: function(string, callback) {
			var studentSearchResult = [];
			var percentSign = "%";
			var editedString = percentSign.concat("", string, percentSign);
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM students WHERE name LIKE ?;', [editedString], function(tx, res){
					for (var i = 0; i < res.rows.length; i++){
						studentSearchResult.push(res.rows.item(i));
					}
				});
			}, function(error){
				console.log("searchStudents ERROR: " + error.message);
				callback(true, null); //there was an error, and so there are no students to return.
			}, function(){
				console.log("searchStudents succeded!");
				callback(false, studentSearchResult);
			});
		},

		//adds a student to the already open attendance report. Essentially the "here" button.
		addStudentToReport: function(reportUID, studentUID, callback){
			db.transaction(function(tx){
				tx.executeSql('INSERT INTO individualRecords (reportUID, studentUID, didEat) VALUES (?, ?, 1);', [reportUID, studentUID]);
			}, function(error){
				console.log("Student record ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Student record succeded!");
				callback(false); //there was not an error
			});
		},

		removeIndividualRecord: function(individualUID, callback){
			db.transaction(function(tx){
				tx.executeSql('DELETE FROM individualRecords WHERE uid = ?', [individualUID]);
			}, function(error){
				console.log("Student record deletion ERROR: " + error.message);
				callback(true); //there was an error
			}, function(){
				console.log("Student record deletion succeded!");
				callback(false); //there was not an error
			});
		},

		//search a student from attendece report
		//returns a list of matched student obj with added property individualReport
		getStudentsInReport: function(reportUID, string, callback){
			var studentReportSearchResult = [];
			var percentSign = "%"
			var editedString = percentSign.concat("", string, percentSign);
			db.transaction(function(tx){
				tx.executeSql('SELECT * FROM individualRecords WHERE name LIKE ?;', [editedString], function(tx, res){
					for (var i = 0; i < res.rows.length; i++){
						studentReportSearchResult.push(res.rows.item(i));
						tx.executeSql('SELECT * FROM individualRecords WHERE reportUID = ? AND studentUID = ?;', [reportUID, res.rows.item(i).uid], function (tx, res2){
							studentReportSearchResult[i].individualReport = res2.rows.item(0);
						});
					}
				});
			}, function(error){
				console.log("Get students from report ERROR: " + error.message);
				callback(true, null);
			}, function(){
				console.log("Get students from report succeded!");
				callback(false, studentReportSearchResult);
			});
		},

		//adds a comment to a students record for the day.
		setComment: function(recordUID, comment, callback){
			db.transaction(function(tx){
				tx.executeSql('UPDATE individualRecords SET comments = ? WHERE uid = ?;', [comment, recordUID]);
			}, function(error){
				console.log('Comment addition ERROR: ' + error.message);
				callback(true); //there was an error
			}, function(){
				console.log('Comment addition succeded!');
				callback(false); //there was not an error
			});
		},

		//adds a comment to a students record for the day.
		addComment: function(studentUID, comment, callback){
			comment = " " + comment;
			db.transaction(function(tx){
				tx.executeSql('UPDATE individualRecords SET comments = CONCAT(comments, ?) WHERE studentUID = ?;', [comment, studentUID]);
			}, function(error){
				console.log('Comment addition ERROR: ' + error.message);
				callback(true); //there was an error
			}, function(){
				console.log('Comment addition succeded!');
				callback(false); //there was not an error
			});
		},

		// edits a kid's discipline point for the day.
		addDisciplinePoint: function(uid, callback){
			db.transaction(function(tx){
				tx.executeSql('UPDATE individualRecords SET disciplinePoints = disciplinePoints + ? WHERE uid = ?;', [1, uid]);
			}, function(error){
				console.log('editDisciplinePoint ERROR: ' + error.message);
				callback(true); //there was an error
			}, function(){
				console.log('editDisciplinePoint succeded!');
				callback(false); //there was not an error
			});
		},

		getDisciplinePoints: function(uid, callback){
			var disciplinePoints = 0;
			db.transaction(function(tx){
				tx.executeSql('SELECT disciplinePoints FROM individualRecords WHERE uid = ?', [uid], function(tx, res){
					disciplinePoints = res.rows.item(0).disciplinePoints;
				});
			}, function(error){
				console.log('getDisciplinePoints ERROR: ' + error.message);
				callback(true, null); //there was an error
			}, function(){
				console.log('getDisciplinePoint succeded!');
				callback(false, disciplinePoints); //there was not an error
			});
		},

		// reset a kid's discipline point to 0.
		setDisciplinePoint: function(studentUID, disciplinePoint, callback){
			db.transaction(function(tx){
				tx.executeSql('UPDATE individualRecords SET disciplinePoint = ? WHERE studentUID = ?;', [disciplinePoint, studentUID]);
			}, function(error){
				console.log('setDisciplinePoint ERROR: ' + error.message);
				callback(true); //there was an error
			}, function(){
				console.log('setDisciplinePoint succeded!');
				callback(false); //there was not an error
			});
		},

		// add special circumstance information to an existing student
		addSpecialCircumstance: function (studentUID, specialCircumstances, callback) {
			if(specialCircumstances.isChanged == 1){
				db.transaction(function(tx){
					tx.executeSql('SELECT specialCircumstances, isNew FROM students WHERE uid = ?;', [studentUID], function(tx, res){
						if(res.rows.item(0).specialCircumstances){
							specialCircumstances.value = res.rows.item(0).specialCircumstances + "||" + specialCircumstances.value;
						}
						tx.executeSql('UPDATE students SET specialCircumstances = ? WHERE uid = ?;', [specialCircumstances.value, studentUID]);
						if(res.rows.item(0).isNew != 1){
							tx.executeSql('SELECT * FROM studentMods WHERE studentUID = ?;', [studentUID], function(tx, res){
								if(res.rows.length > 0){
									tx.executeSql('UPDATE studentMods SET specialCircumstancesChange = ? WHERE studentUID = ?;', [1, studentUID]);
								}
								else{
									tx.executeSql('INSERT INTO studentMods (studentUID, specialCircumstancesChange) VALUES (?, ?);', [studentUID, 1]);
								}
							});
						}
					});
				}, function(error){
					console.log('specialCircumstances addition ERROR: ' + error.message);
					callback(true); //there is an error
				}, function(){
					console.log('specialCircumstances addition succeded!');
					callback(false); // there wasn't an error
				});
			}
			else{
				callback(false); //there wasn't an error
			}
		},

		getSpecialCircumstances: function(studentUID, callback){
			var specialCircumstances = "";
			db.transaction(function(tx){
				tx.executeSql('SELECT specialCircumstances FROM students WHERE uid = ?;', [studentUID], function(tx, res){
					if(res.rows.item(0).specialCircumstances){
						specialCircumstances = res.rows.item(0).specialCircumstances.split('||').join(' ');
					}
				});
			}, function(error){
				console.log("Getting specialCircumstances ERROR: " + error.message);
				callback(true, null); //there was an error
			}, function(){
				console.log("Getting specialCircumstances succeded!");
				callback(false, specialCircumstances);
			});
		},

		// record whether the student ate the snack or not 
		didEat: function(recordUID, didEat, callback){
			db.transaction(function(tx){
				tx.executeSql('UPDATE individualRecords SET didEat = ? WHERE uid = ?;', [didEat, recordUID]);
			}, function(error){
				console.log('eating record ERROR: ' + error.message);
				callback(true); //there was an error
			}, function(){
				console.log('eating record succeded!');
				callback(false); //there was not an error
			});
		}
	};
	
	databaseInteractor.preliminaryInitialize(function(err){
		if(!err){
			callback(false, databaseInteractor); //there was not an error, and so here is the databaseInteractor fully constructed
		}
		else{
			callback(true, null); //there was an error and so there is no database interactor to return
		}
	});
	
}
