### Description
This is an app designed by the St. Anne's-Belfield HSE class of 2018-2019 for the Honduran Child Alliance. It is meant to give the HCA a centralized area with all of their attendsance data.

### IOS Installation

- Run ``sudo gem install cocoapods``
- Run ``npm install -g cordova``
- Run ``npm install``
- Run ``cordova build ios``
- Open  ``/platforms/ios/HCA.xcworkspace``
- Hit the play button in xcode, and enjoy your app
